/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2016 King Kévin <kingkevin@cuvoodoo.info> */

/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdio.h> // standard I/O facilities
#include <stdlib.h> // standard utilities
#include <unistd.h> // standard streams
#include <errno.h> // error number utilities
#include <string.h> // stting utilities

/* STM32 (including CM3) libraries */
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/cm3/scb.h> // vector table definition
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities

#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>

/* own libraries */
#include "global.h" // board definitions
#include "usart.h" // USART utilities
#include "usb_cdcacm.h" // USB CDC ACM utilities
#include "vfd.h" // VFD driver

/* default output (i.e. for printf) */
int _write(int file, char *ptr, int len)
{
	int i;

	if (file == STDOUT_FILENO || file == STDERR_FILENO) {
		for (i = 0; i < len; i++) {
			if (ptr[i] == '\n') { // add carrier return before line feed. this is recommended for most UART terminals
				usart_putchar_nonblocking('\r'); // a second line feed doesn't break the display
				cdcacm_putchar('\r'); // a second line feed doesn't break the display
			}
			usart_putchar_nonblocking(ptr[i]); // send byte over USART
			cdcacm_putchar(ptr[i]); // send byte over USB
		}
		return i;
	}
	errno = EIO;
	return -1;
}

static void gpio_setup(void)
{
	rcc_periph_clock_enable(LED_RCC); //enable clock for LED
	gpio_set_mode(LED_PORT, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, LED_PIN); // set LED pin to 'output push-pull'	
}

int main(void)
{	
	SCB_VTOR = (uint32_t) 0x08002000; // relocate vector table because of the bootloader
	rcc_clock_setup_in_hse_8mhz_out_72mhz(); // use 8 MHz high speed external clock to generate 72 MHz internal clock

	gpio_setup(); // setup main inputs/outputs
	usart_setup(); // setup USART (for printing)
	cdcacm_setup(); // setup USB CDC ACM (for printing)	
	vfd_setup(); // setup VFD
	
	setbuf(stdout, NULL); // set standard out buffer to NULL to immediately print	
	setbuf(stderr, NULL); // set standard error buffer to NULL to immediately print

	printf("welcome to the STM32F1 CuVoodoo vacuum fluorescent display driver\n");

	printf("testing VFD\n");
	vfd_on(); // switch on VFD
	vfd_test(); // show all anodes
	for (uint32_t i = 0; i < 0x800000; i++) { // show for a bit of time
		__asm__("nop");
	}
	vfd_clear(); // clear all data

	printf("scrolling message\n");
	const char* message = "            follow the white rabbit  "; // message to display (shifted by left-padded by VFD_MATRIX (12), right padded by VFD_MATRIX-VFD_DIGITS (2))
	for (uint8_t i=0; i<strlen(message); i++) { // scroll through message (from right to left)
		vfd_clear(); // clear display
		for (uint8_t j=i; j<strlen(message); j++) { // display message
			if (j+2<(uint8_t)strlen(message)) { // align VFD_MATRIX (12) to VFD_DIGITS (10)
				vfd_digit(j-i,message[j+2]); // shift the matrix display
			}
			vfd_matrix(j-i,message[j]); // show digit
		}
		for (uint32_t j = 0; j < 0x200000; j++) { // show for a bit of time
			__asm__("nop");
		}
	}
	vfd_clear(); // clear all data
	
	printf("showing animation\n");
	// show rabbit + number animation
	const uint8_t animation[] = {2,3,2,3,2,3,7,6,7,6,7,6}; // rabbit animation (using pict5x7)
	for (uint8_t i=0; i<sizeof(animation) && i<VFD_MATRIX; i++) {
		vfd_clear(); // clear display
		vfd_matrix(i,0x80+animation[i]); // show rabbit
		for (uint8_t j=0; j<50; j++) {
			for (uint8_t k=0; k<VFD_DIGITS; k++) {
				vfd_digit(k,'0'+(rand()%10)); // show (not very) random number
				for (uint32_t l = 0; l<0x4000; l++) { // show for a bit of time
					__asm__("nop");
				}
			}
		}
	}
	vfd_clear(); // clear all data

	printf("type to display\n");
	uint8_t vfd_place = 0; // the place on which the next character will be displayed on the VFD
	bool vfd_flag = false; // do something with the VFD
	char c = 0; // the character to display
	char previous_line[VFD_DIGITS] = {0};
	while (true) {
		while (usart_received) { // echo every received character
			c = usart_getchar();
			vfd_flag = true; // display character
		}
		while (cdcacm_received) { // echo every received character
			c = cdcacm_getchar();
			vfd_flag = true; // display character
		}
		while (vfd_flag) {
			vfd_flag = false;
			printf("%c", c); // transmit receive character
			gpio_toggle(LED_PORT, LED_PIN); // toggle LED to show activity
			if (c=='\r' || c=='\n') { // on (new) newline
				if (vfd_place>0) { // only on new newline
					vfd_clear(); // clear display
					vfd_place = 0; // restart from beginning
					// show previous line on digits (and clear it)
					for (uint8_t i=0; i<LENGTH(previous_line) && i<VFD_DIGITS; i++) {
						vfd_digit(i,previous_line[i]);
						previous_line[i] = 0;
					}
				}
			} else {
				// show typed character for current line on matrix
				if (vfd_place < VFD_MATRIX) {
					vfd_matrix(vfd_place,c); // display character on digit
				}
				// save character for the net previous line
				if (vfd_place < LENGTH(previous_line)) {
					previous_line[vfd_place] = c;
				}
				vfd_place++; // go to next place (you can overflow it and confuse the newline)
			}
		}	
		__WFI(); // go to sleep
	}

	return 0;
}
