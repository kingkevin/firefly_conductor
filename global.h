/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2016 King Kévin <kingkevin@cuvoodoo.info> */

/* get the length of an array */
#define LENGTH(x) (sizeof(x) / sizeof((x)[0]))

/* system clock frequency in Hz */
#define SYSTEM_CLOCK_FREQ 72000000

/* LED is on pin 11/PA1 */
#define LED_RCC RCC_GPIOA
#define LED_PORT GPIOA
#define LED_PIN	GPIO1

/* default output (i.e. for printf) */
int _write(int file, char *ptr, int len);

