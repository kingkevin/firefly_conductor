## This library is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this library.  If not, see <http://www.gnu.org/licenses/>.
##

## this file is based on Makefile from libopencm3 examples
## available at https://github.com/libopencm3/libopencm3-examples

# be silent per default, but 'make V=1' will show all compiler calls.
ifneq ($(V),1)
Q := @
NULL := 1> /dev/null 2> /dev/null
endif

# the final binary name (without extension)
BINARY = firmware

# source files
LIBS = lib
CSRC = $(wildcard *.c)
CSRC += $(foreach LIB,$(LIBS),$(wildcard $(LIB)/*.c))
OBJ = $(patsubst %.c,%.o,$(CSRC))

# executables
PREFIX ?= arm-none-eabi
CC := $(PREFIX)-gcc
CXX := $(PREFIX)-g++
LD := $(PREFIX)-gcc
AR := $(PREFIX)-ar
AS := $(PREFIX)-as
OBJCOPY := $(PREFIX)-objcopy
OBJDUMP := $(PREFIX)-objdump
GDB := $(PREFIX)-gdb

# opencm3 libraries
OPENCM3_DIR := libopencm3
INCLUDE_DIR = $(OPENCM3_DIR)/include
LIB_DIR = $(OPENCM3_DIR)/lib
SCRIPT_DIR = $(OPENCM3_DIR)/scripts

# verify if libopencm3 has been downloaded
OPENCM3_DIR_EXISTS = $(shell [ -d $(OPENCM3_DIR) ] && echo 1 || echo 0 )
ifeq ($(OPENCM3_DIR_EXISTS), 0)
$(info run "git submodule init" and "git submodule update" before runnig make)
$(error libopencm3 repository is not initialized)
endif

# device flag
DEFS += -DSTM32F1

# C flags
CFLAGS += -Os -g
CFLAGS += -Wall -Werror -Wundef -Wextra -Wshadow -Wimplicit-function-declaration
CFLAGS += -Wredundant-decls -Wmissing-prototypes -Wstrict-prototypes
CFLAGS += -fno-common -ffunction-sections -fdata-sections
CFLAGS += -MD
CFLAGS += -I. -I$(INCLUDE_DIR) $(patsubst %,-I%,$(LIBS))
CFLAGS += $(DEFS)

# linker script
LDSCRIPT = stm32f103x8-dfu.ld

# linker flags
LDFLAGS += --static -nostartfiles
LDFLAGS += -L$(LIB_DIR)
LDFLAGS += -I. $(patsubst %,-I%,$(LIBS))
LDFLAGS += -T$(LDSCRIPT)
LDFLAGS += -Wl,-Map=$(*).map
LDFLAGS += -Wl,--gc-sections
ifeq ($(V),99)
LDFLAGS += -Wl,--print-gc-sections
endif

# used libraries
LIBNAME = opencm3_stm32f1
LDLIBS += -l$(LIBNAME)
LDLIBS += -Wl,--start-group -lc -lgcc -lnosys -Wl,--end-group

# device specific flags
FP_FLAGS ?= -msoft-float
ARCH_FLAGS = -mthumb -mcpu=cortex-m3 $(FP_FLAGS) -mfix-cortex-m3-ldrd

# OpenOCD configuration
OOCD ?= openocd
OOCD_INTERFACE ?= stlink-v2
OOCD_TARGET ?= stm32f1x

# which USB CDC ACM port is used bu the device, so we can reset it
ACMPORT = /dev/ttyACM0

# board specific USB DFU bootloader
BOOTLOADERS = STM32duino-bootloader
BOOTLOADER = $(BOOTLOADERS)/STM32F1/binaries/generic_boot20_pa1.bin

# verify if STM32duino-bootloader has been downloaded
BOOTLOADER_EXISTS = $(shell [ -f $(BOOTLOADER) ] && echo 1 || echo 0 )
ifeq ($(BOOTLOADER_EXISTS), 0)
$(info run "git submodule init" and "git submodule update" before runnig make)
$(error STM32duino-bootloader repository is not initialized)
endif

# compile target rules
all: elf

elf: $(BINARY).elf
bin: $(BINARY).bin
hex: $(BINARY).hex
srec: $(BINARY).srec
list: $(BINARY).list

%.bin: %.elf
	$(Q)$(OBJCOPY) -Obinary $(<) $(@)

%.hex: %.elf
	$(Q)$(OBJCOPY) -Oihex $(<) $(@)

%.srec: %.elf
	$(Q)$(OBJCOPY) -Osrec $(<) $(@)

%.list: %.elf
	$(Q)$(OBJDUMP) -S $(<) > $(@)

%.map: %.elf
	@# it's generated along with the elf

%.elf: $(OBJ) $(LDSCRIPT) $(LIB_DIR)/lib$(LIBNAME).a
	$(info compiling $(@))
	$(Q)$(LD) $(LDFLAGS) $(ARCH_FLAGS) $(OBJ) $(LDLIBS) -o $(@)
	$(Q)size $(@)

%.o: %.c
	$(Q)$(CC) $(CFLAGS) $(ARCH_FLAGS) -o $(@) -c $(<)

clean:
	$(Q)$(RM) *.o *.d *.elf *.bin *.hex *.srec *.list *.map

# make libopencm3 if not done
$(LIB_DIR)/lib$(LIBNAME).a:
	$(info compiling libopencm3 library)
	$(Q)$(MAKE) -C $(OPENCM3_DIR)

bootloader: $(BOOTLOADER)
	$(info flashing USB DFU bootloader $(<))
	$(Q)$(OOCD) --file interface/$(OOCD_INTERFACE).cfg --file target/$(OOCD_TARGET).cfg --command "init" --command "reset init" --command "flash write_image erase $(<) 0x08000000" --command "reset" --command "shutdown" $(NULL)

flash: flash-swd

flash-swd: $(BINARY).hex
	$(info flashing $(<) using SWD)
	$(Q)$(OOCD) --file interface/$(OOCD_INTERFACE).cfg --file target/$(OOCD_TARGET).cfg --command "init" --command "reset init" --command "flash write_image erase $(<)" --command "reset" --command "shutdown" $(NULL)

# reset device by setting the data width to 5 bis on the USB CDC ACM port
reset:
	$(Q)stty --file /dev/ttyACM0 115200 raw cs5
	$(Q)sleep 0.5
	
flash-dfu: $(BINARY).bin reset
	$(info flashing $(<) using DFU)
	$(Q)dfu-util --device 1eaf:0003 --cfg 1 --intf 0 --alt 2 --reset --download $(<) $(NULL)

.PHONY: clean elf bin hex srec list bootloader flash flash-swd flash-dfu
.SECONDARY:
-include $(OBJ:.o=.d)
