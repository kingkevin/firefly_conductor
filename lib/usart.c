/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2016 King Kévin <kingkevin@cuvoodoo.info> */
/* this library handles the USART */

/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdio.h> // standard I/O facilities
#include <stdlib.h> // general utilities

/* STM32 (including CM3) libraries */
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/usart.h> // universal synchronous asynchronous receiver transmitter library
#include <libopencm3/cm3/nvic.h> // interrupt handler
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities

#include "usart.h" // USART header and definitions

/* input and output ring buffer, indexes, and available memory */
static uint8_t rx_buffer[USART_BUFFER] = {0};
static volatile uint8_t rx_i = 0;
static volatile uint8_t rx_used = 0;
static uint8_t tx_buffer[USART_BUFFER] = {0};
static volatile uint8_t tx_i = 0;
static volatile uint8_t tx_used = 0;
/* show the user how much data received over USART is ready */
volatile uint8_t usart_received = 0; // same as rx_used, but since the user can write this variable we don't rely on it

/* setup USART port */
void usart_setup(void)
{
	rcc_periph_clock_enable(USART_RCC); // enable USART1 clock
	gpio_set_mode(USART_PORT, GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, USART_PIN_TX); // setup GPIO pin USART transmit
	gpio_set_mode(USART_PORT, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, USART_PIN_RX); // setup GPIO pin USART receive

	/* setup UART parameters */
	usart_set_baudrate(USART, 115200);
	usart_set_databits(USART, 8);
	usart_set_stopbits(USART, USART_STOPBITS_1);
	usart_set_mode(USART, USART_MODE_TX_RX);
	usart_set_parity(USART, USART_PARITY_NONE);
	usart_set_flow_control(USART, USART_FLOWCONTROL_NONE);

	nvic_enable_irq(USART_IRQ); // enable the USART interrupt
	usart_enable_rx_interrupt(USART); // enable receive interrupt
	usart_enable(USART); // enable USART

	/* reset buffer states */
	tx_i = 0;
	tx_used = 0;
	rx_i = 0;
	rx_used = 0;
	usart_received = 0;
}

/* put character on USART (blocking) */
void usart_putchar_blocking(char c)
{
	if (c == '\n') { // add carrier return before line feed. this is recommended for most UART terminals
		usart_putchar_blocking('\r'); // a second carrier return doesn't influence the terminal
	}
	usart_flush(); // empty buffer first
	usart_send_blocking(USART, c); // send character
}

/* ensure all data has been transmitted (blocking) */
void usart_flush(void)
{
	while (tx_used) { // idle until buffer is empty
		__WFI(); // sleep until interrupt
	}
	usart_wait_send_ready(USART); // wait until transmit register is empty (transmission might not be complete)
}

/* get character from USART (blocking) */
char usart_getchar(void)
{
	while (!rx_used) { // idle until data is available
		__WFI(); // sleep until interrupt;
	}
	char to_return = rx_buffer[rx_i]; // get the next available character
	rx_i = (rx_i+1)%sizeof(rx_buffer); // update used buffer
	rx_used--; // update used buffer
	usart_received = rx_used; // update available data
	return to_return;
}

/* put character on USART (non-blocking until buffer is full) */
void usart_putchar_nonblocking(char c)
{
	while (tx_used>=sizeof(tx_buffer)) { // idle until buffer has some space
		__WFI();
	}
	tx_buffer[(tx_i+tx_used)%sizeof(tx_buffer)] = c; // put character in buffer
	tx_used++; // update used buffer
	usart_enable_tx_interrupt(USART); // enable transmit interrupt
}

#if (USART==USART1)
void usart1_isr(void)
#elif (USART==USART2)
void usart2_isr(void)
#elif (USART==USART3)
void usart3_isr(void)
#endif
{ // USART interrupt
	if (usart_get_interrupt_source(USART, USART_SR_TXE)) { // data has been transmitted
		if (!tx_used) { // no data in the buffer to transmit
			usart_disable_tx_interrupt(USART); // disable transmit interrupt
		} else {
			usart_send(USART,tx_buffer[tx_i]); // put data in transmit register
			tx_i = (tx_i+1)%sizeof(rx_buffer); // update location on buffer
			tx_used--; // update used size
		}
	}
	if (usart_get_interrupt_source(USART, USART_SR_RXNE)) { // data has been received
		// only save data if there is space in the buffer
		if (rx_used>=sizeof(rx_buffer)) {
			usart_recv(USART); // read to clear interrupt
		} else {
			rx_buffer[(rx_i+rx_used)%sizeof(rx_buffer)] = usart_recv(USART); // put character in buffer
			rx_used++; // update used buffer
			usart_received = rx_used; // update available data
		}
	}
}
