/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2016 King Kévin <kingkevin@cuvoodoo.info> */
/* this library is used to drive the vacuum fluorescent display extracted from a Samsung SER-6500 cashier machine
 * it used three chained supertex HV518P shift register VFD drivers */

/* the number of blocks available on the VFD */
#define VFD_DRIVERS 3
#define VFD_DIGITS 10
#define VFD_MATRIX 12

/* set digit <nb> to ASCII character <c>
 * use the MSB of <c> to enable the dot */
void vfd_digit(uint8_t nb, char c);
/* set dot matrix <nb> to ASCII character <c>
 * non ASCII characters are used for pictures */
void vfd_matrix(uint8_t nb, char c);
/* clear VFD display */
void vfd_clear(void);
/* test VFD display (light up all anodes) */
void vfd_test(void);
/* transmit every digit and matrix */
void vfd_on(void);
/* switch VFD display off */
void vfd_off(void);
/* setup VFD */
void vfd_setup(void);
