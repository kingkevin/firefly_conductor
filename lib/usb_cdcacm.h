/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2016 King Kévin <kingkevin@cuvoodoo.info> */
/* this library handles the USB CDC ACM */


/* RX buffer size */
#define CDCACM_BUFFER 128
/* show the user how much received is available */
extern volatile uint8_t cdcacm_received;

/* setup USB CDC ACM */
void cdcacm_setup(void);
/* get character from USB CDC ACM (blocking) */
char cdcacm_getchar(void);
/* put character on USB CDC ACM (blocking) */
void cdcacm_putchar(char c);
