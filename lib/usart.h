/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2016 King Kévin <kingkevin@cuvoodoo.info> */
/* this library handles the USART */

/* which USART to use */
#define USART USART1
#define USART_RCC RCC_USART1
#define USART_IRQ NVIC_USART1_IRQ
#define USART_PORT GPIOA
#define USART_PIN_TX GPIO_USART1_TX
#define USART_PIN_RX GPIO_USART1_RX

/* serial baudrate, in bits per second (with 8N1 8 bits, no parity bit, 1 stop bit settings) */
#define BAUD 115200
/* RX and TX buffer sizes */
#define USART_BUFFER 128
/* show the user how much received is available */
extern volatile uint8_t usart_received;

/* setup USART port */
void usart_setup(void);
/* put character on USART (blocking) */
void usart_putchar_blocking(char c);
/* ensure all data has been transmitted (blocking) */
void usart_flush(void);
/* get character from USART (blocking) */
char usart_getchar(void);
/* put character on USART (non-blocking until buffer is full) */
void usart_putchar_nonblocking(char c);
